import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { config } from 'rxjs';
import { AppConfigService } from 'src/app/services/app-config.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    public appConfig: AppConfigService
  ) { }

  ngOnInit(): void {
  }

  openLogin(): void{
    let config: MatDialogConfig = new MatDialogConfig();
    this.dialog.open(LoginComponent, {
      id: 'login-modal-component',
      width: '100%'
    });
  }

}
