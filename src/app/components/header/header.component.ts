import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { drawer } from 'src/app/animations/drawer-animation';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations:[drawer]
})
export class HeaderComponent implements OnInit {

  drawerState: string = 'close'

  @Output() selected: EventEmitter<string> = new EventEmitter<string>()

  constructor(
    public appConfig: AppConfigService
  ) { }

  ngOnInit(): void {
  }

  openDrawer(): void{
    this.drawerState = 'open';
  }
  closeDrawer(): void{
    this.drawerState = 'close';
  }

  onSelected(obj: string): void{
    console.log(obj);
    this.selected.emit(obj);
  }

}
