import { AvisServiceService } from './../../services/avis-service.service';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Jaime } from 'src/app/entities/jaime';
import { Oeuvre } from 'src/app/entities/oeuvre';
import { AppConfigService } from 'src/app/services/app-config.service';
import { HttpURLs } from 'src/app/utilities/http-urls';
import { LoginComponent } from '../login/login.component';
import { VisualArticleComponent } from '../visual-article/visual-article.component';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-article-box',
  templateUrl: './article-box.component.html',
  styleUrls: ['./article-box.component.scss']
})
export class ArticleBoxComponent implements OnInit {

  @Input() data: Oeuvre;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private appConfig: AppConfigService,
    private service: AvisServiceService
  ) { }

  ngOnInit(): void {
  }

  openVisual(): void{
    this.dialog.open(VisualArticleComponent, {
      id: 'login-modal-component',
      width: '100%',
      data: this.data
    });
  }

  getImage(id: number){
    return HttpURLs.URL_GET_OEUVRE_IMAGE(id);
  }

  viewArtiste(id: number){
    this.router.navigate(['/artiste'], {
      queryParams: {
        id: id
      }
    })
  }

  like(): void{
    if(!this.appConfig.user){
      this.dialog.open(LoginComponent, {
        id: 'login-modal-component',
        width: '100%'
      });

      return;
    }
    let jaime: Jaime = new Jaime();
    jaime.oeuvre = this.data;
    jaime.user = this.appConfig.user;

    let dat = {
      jaime: jaime
    }

    this.service.likeOeuvre(dat).then(res => {
      this.data.jaimes.push(res.response);
    }).catch(error => Utility.openError(error))


  }

}
