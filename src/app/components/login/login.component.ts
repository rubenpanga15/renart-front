import { AuthServiceService } from './../../services/auth-service.service';
import { Component, OnInit } from '@angular/core';
import { fade, fade_left, fade_right } from 'src/app/animations/drawer-animation';
import { AppConfigService } from 'src/app/services/app-config.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Utility } from 'src/app/utilities/utility';
import { User } from 'src/app/entities/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [fade, fade_left, fade_right]
})
export class LoginComponent implements OnInit {

 login = {
    username: '',
    password: ''
  }

  constructor(
    private service: AuthServiceService,
    private appConfig: AppConfigService,
    public dialog: MatDialog,
    private dialogRef: MatDialogRef<LoginComponent>
  ) { }

  login_state = 'in';
  inscription_state = 'out';

  ngOnInit(): void {
  }

  changeScreen(screen: string): void{
    if(screen == 'login'){
      this.login_state = 'in';
      this.inscription_state = 'out';
    }else{
      this.login_state = 'out';
      this.inscription_state = 'in';
    }
  }

  connexion(): void{
    this.service.connexion(this.login).then(
      res => {
        this.dialogRef.close(true);
      }
    ).catch(
      error => Utility.openWarn(error)
    )
  }

  user:User = new User();

  confrmPassword: string = ""

  register(): void{
    if(this.confrmPassword != this.user.password){
      Utility.openError("Les mots de passe ne correspondent pas");
      return;
    }
    this.service.signUp(this.user).then(
      res => {
        this.dialogRef.close(true);
      }
    ).catch(
      error => Utility.openError(error)
    )
  }

}
