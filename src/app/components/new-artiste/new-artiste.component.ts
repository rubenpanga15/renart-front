import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FileSelecterComponent } from 'src/app/dialogs/file-selecter/file-selecter.component';
import { Artiste } from 'src/app/entities/artiste';
import { AppConfigService } from 'src/app/services/app-config.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-new-artiste',
  templateUrl: './new-artiste.component.html',
  styleUrls: ['./new-artiste.component.scss']
})
export class NewArtisteComponent implements OnInit {

  artiste: Artiste = new Artiste();

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  save(){
    this.appConfig.saveArtiste(this.artiste).then(res => Utility.openSuccess("Artiste enregistré avec succès")).catch(error => {
      Utility.openError(error);
    })
  }

  data: string;

  openFileSelecter(): void{
    this.dialog.open(FileSelecterComponent, {
      width: '30%'
    }).afterClosed().subscribe(
      (res) => {
        console.log(res)
        this.artiste.profilData = res.data;
        this.artiste.profilExtension = res.extension;
        this.data = res.file;
      }
    )
  }

  removePhoto(): void{
    this.artiste.profilData = undefined;
        this.artiste.profilExtension = undefined;
        this.data = undefined;
  }

}
