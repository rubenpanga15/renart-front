import { SelecterDialogComponent } from 'src/app/dialogs/selecter-dialog/selecter-dialog.component';
import { BasicCallback } from 'src/app/callbacks/basic-callback';
import { OeuvrePhoto } from './../../entities/oeuvre-photo';
import { FileSelecterComponent } from './../../dialogs/file-selecter/file-selecter.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Oeuvre } from 'src/app/entities/oeuvre';
import { AppConfigService } from 'src/app/services/app-config.service';
import { Utility } from 'src/app/utilities/utility';
import { Artiste } from 'src/app/entities/artiste';
import { CategorieOeuvre } from 'src/app/entities/categorie-oeuvre';

@Component({
  selector: 'app-new-expostion',
  templateUrl: './new-expostion.component.html',
  styleUrls: ['./new-expostion.component.scss']
})
export class NewExpostionComponent implements OnInit {

  oeuvrePhotos: OeuvrePhoto[] = [];
  oeuvre: Oeuvre = new Oeuvre();

  artistes: Artiste[];
  categorieOeuvres: CategorieOeuvre[];

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService
  ) { }

  ngOnInit(): void {
    this.appConfig.getArtistes().then(
      res => this.artistes = res.response
    );
    this.appConfig.getCategoriesOeuvre().then(
      res=> {
        console.log(res)
        this.categorieOeuvres = res.response
      }
    )
  }

  openFileSelecter(): void{
    this.dialog.open(FileSelecterComponent, {
      width: '30%'
    }).afterClosed().subscribe(
      (res: OeuvrePhoto) => {
        console.log(res)
        this.oeuvre.oeuvrePhotos.push(res)
      }
    )
  }

  callbackArtiste: BasicCallback = {
    onSuccess: (obj) => {
      this.oeuvre.artiste = this.artistes.find(e => e.id == obj.id)
    }
  }

  callbackCategorie: BasicCallback = {
    onSuccess: (obj) => {
      this.oeuvre.categorieOeuvre = this.categorieOeuvres.find(e => e.id == obj.id)
    }
  }

  removePhoto(i: number): void{
    console.log(i);
    this.oeuvre.oeuvrePhotos.splice(i, 1)
  }

  save(): void{
    this.appConfig.saveOeuvre(this.oeuvre).then(
      res => Utility.openSuccess("Success")
    ).catch(error => Utility.openError(error))
  }

  openSelecterCategorie(callback: BasicCallback){
    if(!this.categorieOeuvres)
      return;
    this.dialog.open(SelecterDialogComponent, {
      width: '45%',
      height: '45%',
      data: {
        data: this.categorieOeuvres.map(e => {
          return {
            title: e.description,
            id: e.id
          }
        }),
        callback: callback
      }
    })
  }

  openSelecterArtistes(callback: BasicCallback){
    if(!this.artistes)
      return;
    this.dialog.open(SelecterDialogComponent, {
      width: '45%',
      height: '45%',
      data: {
        data: this.artistes.map(e => {
          return {
            title: e.nom + ' ' + e.postnom +  ' '+e.prenom,
            id: e.id
          }
        }),
        callback: callback
      }
    })
  }



}
