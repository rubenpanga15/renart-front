import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Artiste } from 'src/app/entities/artiste';
import { Oeuvre } from 'src/app/entities/oeuvre';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-voir-artiste',
  templateUrl: './voir-artiste.component.html',
  styleUrls: ['./voir-artiste.component.scss']
})
export class VoirArtisteComponent implements OnInit {
  artiste: Artiste

  oeuvres: Oeuvre[] = []

  constructor(
    public appConfig: AppConfigService,
    private activated: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.appConfig.getArtiste(this.activated.snapshot.queryParams.id)
    .then(
      res => {
        this.artiste = res.response
        this.appConfig.getOeuvresByArtiste(this.artiste.id).then(res => this.oeuvres = res.response)
      }
    )
  }

}
