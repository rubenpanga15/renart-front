import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Artiste } from 'src/app/entities/artiste';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-nos-artistes',
  templateUrl: './nos-artistes.component.html',
  styleUrls: ['./nos-artistes.component.scss']
})
export class NosArtistesComponent implements OnInit {

  artistes: Artiste[]

  constructor(
    public appConfig: AppConfigService,
    private dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.appConfig.getArtistes().then(
      res => this.artistes = res.response
    )
  }

  voirArtiste(id: number): void{
    this.router.navigate(['artiste'], {
      queryParams: {
        id: id
      }
    })
  }

}
