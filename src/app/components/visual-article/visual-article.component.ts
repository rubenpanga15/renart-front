import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Jaime } from 'src/app/entities/jaime';
import { Oeuvre } from 'src/app/entities/oeuvre';
import { AppConfigService } from 'src/app/services/app-config.service';
import { AvisServiceService } from 'src/app/services/avis-service.service';
import { HttpURLs } from 'src/app/utilities/http-urls';
import { Utility } from 'src/app/utilities/utility'
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-visual-article',
  templateUrl: './visual-article.component.html',
  styleUrls: ['./visual-article.component.scss']
})
export class VisualArticleComponent implements OnInit {


  data: any;
  constructor(
    @Inject(MAT_DIALOG_DATA) datum:any,
    private dialogRef: MatDialogRef<VisualArticleComponent>,
    public appConfig: AppConfigService,
    private dialog: MatDialog,
    private service: AvisServiceService
  ) {
    this.data = datum
  }

  ngOnInit(): void {
  }

  getImage(id: number){
    return HttpURLs.URL_GET_OEUVRE_IMAGE(id);
  }

  commande(): void{
    // if(this.data.isDisponible)
    //   Utility.openSuccess('Commande enregistrée avec succès');
    // else
    //   Utility.openWarn("L'oeuvre n'est pas disponible");
  }

  like(oeuvre: Oeuvre): void{
    if(!this.appConfig.user){
      this.dialog.open(LoginComponent, {
        id: 'login-modal-component',
        width: '100%'
      });

      return;
    }
    let jaime: Jaime = new Jaime();
    jaime.oeuvre = oeuvre;
    jaime.user = this.appConfig.user;

    let data = {
      jaime: jaime
    }

    this.service.likeOeuvre(data).then(res => oeuvre.jaimes.push(res.response)).catch(error => Utility.openError(error))


  }

}
