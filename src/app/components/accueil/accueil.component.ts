import { Component, OnInit } from '@angular/core';
import { Oeuvre } from 'src/app/entities/oeuvre';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {

  // data = [
  //   {
  //     src: 'assets/images/1.jpg',
  //     nom: 'Dinde',
  //     type: 'Sculpture',
  //     auteur: 'Ruben Panga',
  //     ville: 'Kinshasa',
  //     prix: '5 $',
  //     isDisponible: false
  //   },
  //   {
  //     src: 'assets/images/2.jpg',
  //     nom: 'Cheval en bois',
  //     type: 'Sculpture',
  //     auteur: 'Appolinaire',
  //     ville: 'Kinshasa',
  //     prix: '10$',
  //     isDisponible: true
  //   },
  //   {
  //     src: 'assets/images/4.jpg',
  //     nom: 'Masque Bantou',
  //     type: 'Sculpture',
  //     auteur: 'Ruben Panga',
  //     ville: 'Kinshasa',
  //     prix: '10$'
  //   },
  //   {
  //     src: 'assets/images/6.jpg',
  //     nom: 'Masque Bantou',
  //     type: 'Sculpture',
  //     auteur: 'Ruben Panga',
  //     ville: 'Kinshasa',
  //     prix: '10$'
  //   },
  //   // {
  //   //   src: 'assets/images/6.jpg',
  //   //   nom: 'Masque Bantou',
  //   //   type: 'Sculpture',
  //   //   auteur: 'Ruben Panga',
  //   //   ville: 'Kinshasa',
  //   //   prix: '10$'
  //   // },
  // ]

  data: Oeuvre[]

  constructor(
    private appConfig: AppConfigService
  ) { }

  ngOnInit(): void {
    this.appConfig.getOeuvres()
    .then(
      res => this.data = res.response
    )
  }

}
