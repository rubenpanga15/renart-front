import { Oeuvre } from 'src/app/entities/oeuvre';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jaimeCount'
})
export class JaimeCountPipe implements PipeTransform {

  transform(value: Oeuvre, ...args: any[]): number {
    let jaimes = 0;
    if(value && value.jaimes)
      jaimes = value.jaimes.length;
    return jaimes;
  }

}
