import { animate, state, style, transition, trigger } from '@angular/animations';

export const drawer = trigger('drawer',
[
    state('close', style({
        transform: 'translateX(-200%)'
    })),
    state('open', style({
        transform: 'translateX(0%)'
    })),
    transition("close <=> open", animate('500ms'))
]);

export const fade = trigger("fade",
    [
        state("in", style({
            opacity: 0
        })),
        state("out", style({
            opacity: 1
        })),
        state("void", style({
            opacity: 0
        })),
        state("*", style({
            opacity: 1
        })),
        transition("out => in", animate("500ms")),
        transition("in => out", animate("500ms"))
    ]
);

export const fade_left = trigger("fade-left",
[
    state("out", style({
        transform: 'translateX(-200%)',
        opacity: 0,
        display: 'none'
    })),
    state("in", style({
        transform: 'translateX(0%)',
        opacity: 1
    })),
    transition("out => in", animate("500ms")),
    transition("in => out", animate("500ms"))
]
);
export const fade_right = trigger("fade-right",
[
    state("out", style({
        transform: 'translateX(200%)',
        opacity: 0,
        display: 'none'
    })),
    state("in", style({
        transform: 'translateX(0%)',
        opacity: 1
    })),
    transition("out => in", animate("500ms")),
    transition("in => out", animate("500ms"))
]
);