import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './components/accueil/accueil.component';
import { MainComponent } from './components/main/main.component';
import { MonCompteComponent } from './components/mon-compte/mon-compte.component';
import { AboutComponent } from './components/about/about.component';
import { LoginAdminComponent } from './components/login-admin/login-admin.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MainAdminComponent } from './components/main-admin/main-admin.component';
import { NewExpostionComponent} from './components/new-expostion/new-expostion.component';
import { NewArtisteComponent } from './components/new-artiste/new-artiste.component';
import { ListExpositionComponent } from './components/list-exposition/list-exposition.component';
import { ListArtistesComponent } from './components/list-artistes/list-artistes.component';
import { NosArtistesComponent } from './components/nos-artistes/nos-artistes.component';
import { VoirArtisteComponent } from './components/voir-artiste/voir-artiste.component';

const routes: Routes = [
  {
    path: '', component: MainComponent,
    children: [
      {
        path: '', component: AccueilComponent,

      },
      {
        path: 'mon-compte', component: MonCompteComponent
      },
      {
        path: 'about', component: AboutComponent
      },
      {
        path: 'artistes', component: NosArtistesComponent
      },
      {
        path: 'artiste', component: VoirArtisteComponent
      }
    ]
  },
  {
    path: 'admin', children: [
      {
        path: '', component: LoginAdminComponent
      },
      {
        path: 'dashboard', component: DashboardComponent
      },
      {
        path: 'main', component: MainAdminComponent,
        children: [
          {
            path: 'new-expo', component: NewExpostionComponent
          },
          {
            path: 'new-artiste', component: NewArtisteComponent
          },
          {
            path: 'list-expo', component: ListExpositionComponent
          },
          {
            path: 'list-artistes', component: ListArtistesComponent
          }
        ]
      }
    ]
  },
  {
    path: '**', redirectTo: '', pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
