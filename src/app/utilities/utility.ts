import Swal from 'sweetalert2';
export class Utility {
    static openWarn(message: string): void{
        Swal.fire({
            icon: 'info',
            text: message
        });
    }

    static openSuccess(message: string): void{
        Swal.fire({
            icon: 'success',
            text: message
        });
    }

    static openError(message: string): void{
        Swal.fire({
            icon: 'error',
            text: message
        });
    }
}
