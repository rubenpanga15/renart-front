export class HttpURLs {
  private static SCHEMA: string = "http://";


  // private static IP: string = "172.16.20.18";
  // private static IP: string = "localhost";
  // private static PORT: string = "8080";

  //Prod
  private static IP: string = "31.207.34.195";
  // private static IP: string = "renartafrika.com";
  private static PORT: string = "8080";

  private static getBase(): string{
    return HttpURLs.SCHEMA + HttpURLs.IP + ":" + HttpURLs.PORT + "/renart/";
  }

  public static URL_CONNEXION = HttpURLs.getBase() + "connexion";
  public static URL_REGISTER = HttpURLs.getBase() + "signup";
  public static URL_UPDATE_PASSWORD = HttpURLs.getBase() + "update-password";

  public static URL_SAVE_OEUVRE = HttpURLs.getBase() + "config/save-oeuvre";
  public static URL_SAVE_ARTISTE = HttpURLs.getBase() + "config/save-artiste";
  public static URL_GET_ARTISTES = HttpURLs.getBase() + "config/get-liste-artistes";
  public static URL_GET_CATEGORIE_OEUVRE = HttpURLs.getBase() + "config/get-liste-categories-oeuvre";
  public static URL_GET_OEUVRES = HttpURLs.getBase() + "config/get-liste-oeuvres";

  public static URL_GET_OEUVRE_IMAGE(id: number){
    return HttpURLs.getBase() + "config/oeuvre-photo/" + `${id}`;
  }

  public static URL_GET_PROFIL_ARTISTE(id: number){
    return HttpURLs.getBase() + "config/artiste-profil/" + `${id}`;
  }

  public static URL_GET_ARTSITE(id: number){
    return HttpURLs.getBase() + "config/artiste/" + `${id}`;
  }

  public static URL_GET_Oeuvre(id: number){
    return HttpURLs.getBase() + "config/oeuvre/" + `${id}`;
  }

  public static URL_GET_OEUVRE_BY_ARTISTE(id: number){
    return HttpURLs.getBase() + "config/oeuvre/find-by-artiste/" + `${id}`;
  }

  public static URL_LIKE_OEUVRE: string = HttpURLs.getBase() + 'avis/like-oeuvre';
}
