export class ErrorResponse {
  static OK:string = "OK";
  static KO: string = "KO";

  errorCode:string;
  errorDescription:string;
}
