import { HttpClient } from '@angular/common/http';
import { AppConfigService } from './app-config.service';
import { Injectable } from '@angular/core';
import { Jaime } from '../entities/jaime';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpURLs } from '../utilities/http-urls';
import { ErrorResponse } from '../utilities/error-response';
import { AppConst } from '../utilities/app-const';

@Injectable({
  providedIn: 'root'
})
export class AvisServiceService {

  constructor(
    private appConfig: AppConfigService,
    private httpClient: HttpClient
  ) { }

  likeOeuvre(data: any): Promise<HttpDataResponse<Jaime>>{
    this.appConfig.onStartWaiting("Operaion en cours");
    return new Promise<HttpDataResponse<Jaime>>((resolve, reject) => {
      this.httpClient.post<HttpDataResponse<Jaime>>(HttpURLs.URL_LIKE_OEUVRE, data)
      .subscribe(
        (result: HttpDataResponse<Jaime>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR)
        }
      )
    })
  }
}
