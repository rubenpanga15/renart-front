import { Artiste } from './../entities/artiste';
import { Oeuvre } from './../entities/oeuvre';
import { Subject } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Injectable } from '@angular/core';
import { User } from '../entities/user';
import { WaitingDialogComponent } from '../dialogs/waiting-dialog/waiting-dialog.component';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpClient } from '@angular/common/http';
import { HttpURLs } from '../utilities/http-urls';
import { ErrorResponse } from '../utilities/error-response';
import { AppConst } from '../utilities/app-const';
import { CategorieOeuvre } from '../entities/categorie-oeuvre';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  user: User
  user$: Subject<User> = new Subject<User>();

  constructor(
    private dialog: MatDialog,
    private httpClient: HttpClient
  ) { }

  onConnected(usr: User): void{
    this.user = usr;
    this.user$.next(usr);
  }

  onDisconnected(): void{
    this.user = undefined;
    this.user$.next(undefined)
  }

  dialogref: MatDialogRef<WaitingDialogComponent>

  onStartWaiting(message?: string, show?: boolean): void{
    if(show)
      this.dialogref = this.dialog.open(WaitingDialogComponent);
  }

  onStopWaiting(): void{
    if(this.dialogref)
      this.dialogref.close();
  }

  saveOeuvre(oeuvre: Oeuvre): Promise<HttpDataResponse<Oeuvre>>{
    this.onStartWaiting("Création de l'oeuvre en cours....", false);
    return new Promise<HttpDataResponse<Oeuvre>>((resolve, reject) => {
      this.httpClient.post<HttpDataResponse<Oeuvre>>(HttpURLs.URL_SAVE_OEUVRE, oeuvre).subscribe(
        (result: HttpDataResponse<Oeuvre>) => {
          this.onStopWaiting()
          if(result.error.errorCode == ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error =>{
          this.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  saveArtiste(artiste: Artiste): Promise<HttpDataResponse<Artiste>>{
    this.onStartWaiting("Création de l'oeuvre en cours....", false);
    return new Promise<HttpDataResponse<Artiste>>((resolve, reject) => {
      this.httpClient.post<HttpDataResponse<Artiste>>(HttpURLs.URL_SAVE_ARTISTE, artiste).subscribe(
        (result: HttpDataResponse<Artiste>) => {
          this.onStopWaiting()
          if(result.error.errorCode == ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error =>{
          this.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  getArtistes(): Promise<HttpDataResponse<Artiste[]>>{
    this.onStartWaiting("Récupération des artistes", true);
    return new Promise<HttpDataResponse<Artiste[]>>((resolve,reject) => {
      this.httpClient.get(HttpURLs.URL_GET_ARTISTES)
      .subscribe(
        (result: HttpDataResponse<Artiste[]>) => {
          this.onStopWaiting();
          if(result.error.errorCode == ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  getOeuvres(): Promise<HttpDataResponse<Oeuvre[]>>{
    this.onStartWaiting("Récupération des artistes", true);
    return new Promise<HttpDataResponse<Oeuvre[]>>((resolve,reject) => {
      this.httpClient.get(HttpURLs.URL_GET_OEUVRES)
      .subscribe(
        (result: HttpDataResponse<Oeuvre[]>) => {
          console.log(result)
          this.onStopWaiting();
          if(result.error.errorCode == ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  getOeuvresByArtiste(id: number): Promise<HttpDataResponse<Oeuvre[]>>{
    this.onStartWaiting("Récupération des oeuvres", true);
    return new Promise<HttpDataResponse<Oeuvre[]>>((resolve,reject) => {
      this.httpClient.get(HttpURLs.URL_GET_OEUVRE_BY_ARTISTE(id))
      .subscribe(
        (result: HttpDataResponse<Oeuvre[]>) => {
          console.log(result)
          this.onStopWaiting();
          if(result.error.errorCode == ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  getCategoriesOeuvre(): Promise<HttpDataResponse<CategorieOeuvre[]>>{
    this.onStartWaiting("Récupération des categories ...", false);
    return new Promise<HttpDataResponse<CategorieOeuvre[]>>((resolve, reject) => {
      this.httpClient.get(HttpURLs.URL_GET_CATEGORIE_OEUVRE)
      .subscribe(
        (res: HttpDataResponse<CategorieOeuvre[]>) => {
          console.log(res)
          this.onStopWaiting();
          if(res.error.errorCode == ErrorResponse.KO)
            reject(res.error.errorDescription);
          resolve(res)
        },
        error => {
          this.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  getArtiste(id: number): Promise<HttpDataResponse<Artiste>>{
    this.onStartWaiting("Recupérationn de l'artiste");
    return new Promise<HttpDataResponse<Artiste>>((resolve, reject) => {
      this.httpClient.get(HttpURLs.URL_GET_ARTSITE(id))
      .subscribe(
        (res: HttpDataResponse<Artiste>) =>{
          this.onStopWaiting();
          if(res.error.errorCode == ErrorResponse.KO)
            reject(res.error.errorDescription);
          resolve(res)
        },
        error => {
          this.onStopWaiting();
          reject(AppConst.NETWORK_ERROR)
        }
      )
    })
  }

  getOeuvre(id: number): Promise<HttpDataResponse<Oeuvre>>{
    this.onStartWaiting("Recupérationn de l'artiste");
    return new Promise<HttpDataResponse<Oeuvre>>((resolve, reject) => {
      this.httpClient.get(HttpURLs.URL_GET_Oeuvre(id))
      .subscribe(
        (res: HttpDataResponse<Oeuvre>) =>{
          this.onStopWaiting();
          if(res.error.errorCode == ErrorResponse.KO)
            reject(res.error.errorDescription);
          resolve(res)
        },
        error => {
          this.onStopWaiting();
          reject(AppConst.NETWORK_ERROR)
        }
      )
    })
  }

  getImageOeuvre(id: number){
    return HttpURLs.URL_GET_OEUVRE_IMAGE(id);
  }

  getProfilArtiste(id: number){
    return HttpURLs.URL_GET_PROFIL_ARTISTE(id);
  }


}
