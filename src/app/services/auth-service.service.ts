import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../entities/user';
import { AppConst } from '../utilities/app-const';
import { ErrorResponse } from '../utilities/error-response';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpURLs } from '../utilities/http-urls';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(
    private appConfig: AppConfigService,
    private httpClient: HttpClient
  ) { }

  public connexion(data: any): Promise<HttpDataResponse<User>>{
    this.appConfig.onStartWaiting("Connexion en cours..", false);

    return new Promise<HttpDataResponse<User>>((resolve, reject) => {
      this.httpClient.post<HttpDataResponse<User>>(HttpURLs.URL_CONNEXION, data)
      .subscribe(
        (result: HttpDataResponse<User>) =>{
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == ErrorResponse.KO)
            reject(result.error.errorDescription);
          this.appConfig.onConnected(result.response);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public signUp(data: User): Promise<HttpDataResponse<User>>{
    this.appConfig.onStartWaiting("Connexion en cours..", false);

    return new Promise<HttpDataResponse<User>>((resolve, reject) => {
      this.httpClient.post<HttpDataResponse<User>>(HttpURLs.URL_REGISTER, data)
      .subscribe(
        (result: HttpDataResponse<User>) =>{
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == ErrorResponse.KO)
            reject(result.error.errorDescription);
          this.appConfig.onConnected(result.response);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public updatePassword(data: any): Promise<HttpDataResponse<boolean>>{
    this.appConfig.onStartWaiting("Connexion en cours..", false);

    return new Promise<HttpDataResponse<boolean>>((resolve, reject) => {
      this.httpClient.post<HttpDataResponse<boolean>>(HttpURLs.URL_REGISTER, data)
      .subscribe(
        (result: HttpDataResponse<boolean>) =>{
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == ErrorResponse.KO)
            reject(result.error.errorDescription);
          resolve(result)
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }
}
