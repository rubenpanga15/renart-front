import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/header/header.component';
import { SharedModule } from './modules/shared/shared.module';
import { WaitingDialogComponent } from './dialogs/waiting-dialog/waiting-dialog.component';
import { BannerComponent } from './components/banner/banner.component';
import { MainComponent } from './components/main/main.component';
import { LoginComponent } from './components/login/login.component';
import { AccueilComponent } from './components/accueil/accueil.component';
import { ArticleBoxComponent } from './components/article-box/article-box.component';
import { FooterComponent } from './components/footer/footer.component';
import { VisualArticleComponent } from './components/visual-article/visual-article.component';
import { MonCompteComponent } from './components/mon-compte/mon-compte.component';
import { AboutComponent } from './components/about/about.component';
import { LoginAdminComponent } from './components/login-admin/login-admin.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderAdminComponent } from './compoents/header-admin/header-admin.component';
import { MainAdminComponent } from './components/main-admin/main-admin.component';
import { NewExpostionComponent } from './components/new-expostion/new-expostion.component';
import { NewArtisteComponent } from './components/new-artiste/new-artiste.component';
import { ListExpositionComponent } from './components/list-exposition/list-exposition.component';
import { ListArtistesComponent } from './components/list-artistes/list-artistes.component';
import { CommentaireBoxComponent } from './components/commentaire-box/commentaire-box.component';
import { MainGuichetComponent } from './components/guichet/main-guichet/main-guichet.component';
import { FileSelecterComponent } from './dialogs/file-selecter/file-selecter.component';
import { SelecterDialogComponent } from './dialogs/selecter-dialog/selecter-dialog.component';
import { NosArtistesComponent } from './components/nos-artistes/nos-artistes.component';
import { VoirArtisteComponent } from './components/voir-artiste/voir-artiste.component';
import { JaimeCountPipe } from './pipes/jaime-count.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    WaitingDialogComponent,
    BannerComponent,
    MainComponent,
    LoginComponent,
    AccueilComponent,
    ArticleBoxComponent,
    FooterComponent,
    VisualArticleComponent,
    MonCompteComponent,
    AboutComponent,
    LoginAdminComponent,
    DashboardComponent,
    HeaderAdminComponent,
    MainAdminComponent,
    NewExpostionComponent,
    NewArtisteComponent,
    ListExpositionComponent,
    ListArtistesComponent,
    CommentaireBoxComponent,
    MainGuichetComponent,
    FileSelecterComponent,
    SelecterDialogComponent,
    NosArtistesComponent,
    VoirArtisteComponent,
    JaimeCountPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
