import { Jaime } from './jaime';
import { Commentaire } from './commentaire';
import { CategorieOeuvre } from './categorie-oeuvre';
import { Artiste } from './artiste';
import { OeuvrePhoto } from './oeuvre-photo';
export class Oeuvre {
  id: number
  nom:string;
  origine: string;
  artiste: Artiste;
  categorieOeuvre: CategorieOeuvre;
  dateCreation: Date
  description: string;
  prix: number;
  status: boolean;
  dateCreat: Date;
  oeuvrePhotos: OeuvrePhoto[] =[];
  commentaires: Commentaire[] = [];
  jaimes: Jaime[] = []
}
