export class CategorieOeuvre {
  id: number;
  description:string;
  code: string;
  status: boolean;
  dateCreat: Date
}
