import { Oeuvre } from "./oeuvre";

export class Artiste {
  id: number;
  nom: string;
  postnom:string;
  prenom:string;
  lieuNaissance: string;
  dateNaissance: Date;
  telephone: string;
  email:string;
  adresse: string;
  biographie: string;
  nationalite:string;
  profile: string;
  profilData: string;
  profilExtension: string;
  status: boolean;
  dateCreat: string;
  oeuvres: Oeuvre[]
}
