import { Oeuvre } from './oeuvre';
export class OeuvrePhoto {

  id: number;
  oeuvre: Oeuvre;
  photoUrl: string;
  file: string;
  data: string;
  extension: string;
  status: boolean;
  dateCreat: string;
}
