export class User {
  id: number;
  noms: string;
  email: string;
  telephone: string;
  password: string;
  role: string;
  status: boolean;
  dateCreat: Date
  username: string;
}
