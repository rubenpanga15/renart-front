import { Oeuvre } from "./oeuvre";
import { User } from "./user";

export class Jaime {
  id: number;
  user: User;
  oeuvre: Oeuvre;
  status: boolean;
  dateCreat: Date;
}
