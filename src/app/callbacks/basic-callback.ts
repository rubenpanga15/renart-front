export interface BasicCallback {
  onSuccess?:(obj?: any) => void;
  onFailed?:(obj?: any) => void;
  notify?: (obj?: any) => void;

}
