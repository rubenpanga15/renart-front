import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-waiting-dialog',
  templateUrl: './waiting-dialog.component.html',
  styleUrls: ['./waiting-dialog.component.scss']
})
export class WaitingDialogComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<WaitingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: string
  ) {
    if(data)
      this.message = data;
   }

  message: string = 'Opération en cours. Veuillez patienter...';

  ngOnInit(): void {
  }

}
