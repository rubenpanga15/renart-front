import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-file-selecter',
  templateUrl: './file-selecter.component.html',
  styleUrls: ['./file-selecter.component.scss']
})
export class FileSelecterComponent implements OnInit {

  ImageBaseData:string | ArrayBuffer=null;

  constructor(
    private dialogRef: MatDialogRef<FileSelecterComponent>
  ) { }

  ngOnInit(): void {
  }

  handleFileInput(files: FileList) {
    let me = this;
    let file = files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      me.ImageBaseData=reader.result;
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
 }

 btnUpload(){
    if(this.ImageBaseData){
      this.dialogRef.close(this.getData(this.ImageBaseData.toString()))
    }else{
      this.dialogRef.close();
    }
  }

  getData(str: string){

    return {
      file: str,
      mimetype: str.substring((str.indexOf(':') + 1), str.indexOf(';')),
      extension: str.substring((str.indexOf('/') + 1), str.indexOf(';')),
      data: str.substring(str.indexOf(',') +1)
    }
  }

}
