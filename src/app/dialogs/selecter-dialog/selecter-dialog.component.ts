import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { BasicCallback } from '../../callbacks/basic-callback';

@Component({
  selector: 'app-selecter-dialog',
  templateUrl: './selecter-dialog.component.html',
  styleUrls: ['./selecter-dialog.component.scss']
})
export class SelecterDialogComponent implements OnInit {

  dataset: any[];
  callback: BasicCallback;

  motclef: string = ''

  constructor(
    private dialogRef: MatDialogRef<SelecterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    //console.log(data)
    this.dataset = data.data;
    this.callback = data.callback;
  }

  filterData(): any[]{

    if(!this.dataset)
      return [];
    return this.dataset.filter(e => e.title.toLowerCase().includes(this.motclef) || e.content.toLowerCase().includes(this.motclef) || e.subcontent.toLowerCase().includes(this.motclef));
  }

  ngOnInit(): void {

  }

  onSelected(obj: any): void{
    //console.log(obj);

    this.dialogRef.close();
    this.callback.onSuccess(obj);
  }

}
